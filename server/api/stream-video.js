import axios from 'axios';

export default defineEventHandler(async (event) => {
  const GOOGLE_DRIVE_FILE_ID = '1XW_zhZzy33iYp7QRX5SWN5jOLp1Qyz78';
  const ACCESS_TOKEN =
    'ya29.a0AeDClZAmTmRvMh-112IPF79_TLQR5n0QIfWjzHdlA_uU94MA3kpDbeWDv69wAy2SCnk95TBCpJvZEHMGMpVZqJKu8Bw_SXLu9ag691prR2tIi5yybz4kmD-6eBmHMs2tK8STv_fIG6D4Z6tpvtqO7RBkuUPmxw1kXzrg1RbWLQaCgYKAegSARISFQHGX2MilYG-ZK577BGdwX8ejKsiLA0177';

  // const range = event.response.headers.range;

  // console.log('rang', range);
  // if (!range) {
  //   setResponseStatus(event, 400);
  //   return { error: 'Requires Range header for streaming' };
  // }

  try {
    const driveUrl = `https://www.googleapis.com/drive/v3/files/${GOOGLE_DRIVE_FILE_ID}?alt=media`;

    const headers = {
      Authorization: `Bearer ${ACCESS_TOKEN}`,
      // Range: range,
    };

    const response = await axios.get(driveUrl, {
      headers,
      responseType: 'stream',
    });

    console.log('Response', response);

    // Set the response headers
    // event.response.setHeader('Content-Type', 'video/mp4');
    // event.response.setHeader('Content-Range', response.headers['content-range']);
    // event.response.setHeader('Accept-Ranges', 'bytes');
    // event.response.setHeader('Content-Length', response.headers['content-length']);

    // Pipe the response data to the response object
    // response.data.pipe(event.response);

    return response.data;
  } catch (error) {
    console.error('Error streaming video:', error);
    setResponseStatus(event, 500);
    return { error: 'Error streaming video' };
  }
});
